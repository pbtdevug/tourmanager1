package com.android.tourmanager;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class LogerService extends /*Intent*/Service {

	Timer startTimer;
	
	@Override
    public IBinder onBind(Intent intent) {
          return null;
    }

    @Override
    public void onCreate() {
          super.onCreate();
          startSheduler();
    }
	
	//private final IBinder mBinder = new MyBinder();
	
	/*public LogerService() {
		super("LogerService");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		startSheduler();
	}*/
	
	private void startSheduler() {
		startTimer = new Timer();
		try {
			// thread wakes up 1 min (60000) after the start of app to start
			// sending pending transactions
			startTimer.scheduleAtFixedRate(new StartTask(),0, 1 * 300000);
			// every after 2 min (120000) one pending transaction is sent to the
			// server until they are all done
		}
		catch (Exception ex) {
			
		}
	}
	
	class StartTask extends TimerTask {
		public void run() {
			try{
				//Toast.makeText(PinActivity,"Service started", Toast.LENGTH_LONG).show();
				Intent intent = new Intent("com.mendhak.gpslogger.GpsLoggingService");
				intent.putExtra("immediatestart", true);
				startService(intent);
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
}
