package com.android.tourmanager;

import android.provider.BaseColumns;

public class TourContract {

	public TourContract(){
		
	}
	
	public static abstract class State implements BaseColumns {
        public static final String TABLE_NAME = "state";
        public static final String SIGNATURE = "signature";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
}
