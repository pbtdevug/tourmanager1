package com.android.tourmanager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PinActivity extends Activity {
	public static final String activityKey = "com.android.PinActivity";
	//private static final String DEBUG_TAG = "PinActivity";
	//private Timer pendingTimer;
	private Handler handler = new Handler(Looper.getMainLooper());
	public SendPending sp = new SendPending();
	public static String pendingFile;
	//private LogerService ls;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin);
		// pendingSheduler();
		createDialog();
		//start GPS Loger
		//startGPSLoger();
		Intent intent = new Intent(this, LogerService.class);
		startService(intent);

		// time out the
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				finish();
			}
		}, 120000);
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pin, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressLint("InflateParams")
	private void createDialog() {
		LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(R.layout.prompts, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInput);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						final String userpin = userInput.getText().toString();
						if(userpin.length()==4){
							startOTPActivity(userpin);
						}
						else{
							Toast.makeText(PinActivity.this,"PIN must be 4 digits!", Toast.LENGTH_LONG).show();
						}
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	private void startOTPActivity(String pin) {
		Intent intent = new Intent(this, OTPActivity.class);
		intent.putExtra(activityKey, pin);
		startActivity(intent);
		finish();
	}

	public void startDialog(View view) {
		createDialog();
	}

	/*private void pendingSheduler() {
		pendingTimer = new Timer();
		try {
			// thread wakes up 1 min (60000) after the start of app to start
			// sending pending transactions
			pendingTimer.scheduleAtFixedRate(new PinActivity.SendPendingTask(),
					60000, 1 * 120000);
			// every after 2 min (120000) one pending transaction is sent to the
			// server until they are all done
		} catch (IllegalStateException ex) {
		} catch (IllegalArgumentException ex) {
		}
	}

	class SendPendingTask extends TimerTask {
		public void run() {
			String[] pend = getSavedFies();
			ArrayList<String> pendFiles = new ArrayList<String>();
			for (int i = 0; i < pend.length; i++) {
				String pendingFile = pend[i];
				if (pend.length > 0 && !pendingFile.equals("history.txt")
						&& !pendingFile.equals("pending.txt")
						&& !pendingFile.equals("store.txt")) {
					pendFiles.add(pendingFile);
				}
				Log.d(DEBUG_TAG, "pendingFile = " + pendingFile);

			}

			if (pendFiles.size() > 0) {
				pendingFile = pendFiles.get(0);
				Log.d(DEBUG_TAG, "SelectedFiles = " + pendingFile);
				String pendingTrxns = readFromFile(pendingFile);
				if (pendingTrxns.length() != 0) {
					new SubmitTask().execute(pendingTrxns);
				}
			}
		}
	}

	private class SubmitTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... xml) {
			return sp.getXMLReply(xml[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			// Log.d(DEBUG_TAG, result);
			checkFeedBack(result);
		}
	}

	private void checkFeedBack(String resp) {
		if (!resp.equals("")) {
			Log.d(DEBUG_TAG, resp);
			try {
				String dir = getFilesDir().getAbsolutePath();
				File file = new File(dir, pendingFile);
				boolean deleted = file.delete();
				Log.v(DEBUG_TAG, "deleted: " + deleted);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private String[] getSavedFies() {
		return getApplicationContext().fileList();
	}

	private String readFromFile(String fileName) {
		String ret = "";

		try {
			InputStream inputStream = openFileInput(fileName);

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.d(DEBUG_TAG, "File not found: " + e.toString());
		} catch (IOException e) {
			Log.d(DEBUG_TAG, "Can not read file: " + e.toString());
		}
		return ret;
	}*/
	
	/*private void startGPSLoger(){
		try{
			Intent intent = new Intent("com.mendhak.gpslogger.GpsLoggingService");
			intent.putExtra("immediatestart", true);
			startService(intent);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
	}*/
}
