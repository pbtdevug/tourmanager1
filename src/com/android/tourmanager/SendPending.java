package com.android.tourmanager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;

public class SendPending {
	public static final String DEBUG_TAG = "SendPending";
	private static final String ns = null;
	
	public String getXMLReply(String request){
		String status = "";
		InputStream strm = makeConnection(request);
		if(strm!=null){
			try {
				try {
					status = parseXML(strm);
				} catch (XmlPullParserException e) {
					System.out.println("XML Exception stack");
					e.printStackTrace();
				}
			} catch (IOException e) {
				System.out.println("IO" +
						" Exception stack");
				e.printStackTrace();
			}
        }
		return status;
	}
	
	private InputStream makeConnection(String xmlStr){
		Log.d(DEBUG_TAG, "XML = "+xmlStr);
		InputStream stream = null;
        try{
        	URL url = new URL(OTPActivity.url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setChunkedStreamingMode(0);
			OutputStream os = new BufferedOutputStream(conn.getOutputStream());
			os.write(xmlStr.getBytes());
			os.flush();
			os.close();
			conn.connect();
			int respCode = conn.getResponseCode();
			Log.d(DEBUG_TAG, "Connection open");
			Log.d(DEBUG_TAG, String.valueOf(respCode));
			if(respCode==200){
				stream = new BufferedInputStream(conn.getInputStream());
				Log.d(DEBUG_TAG, "got inputstream");
				Log.d(DEBUG_TAG, String.valueOf(conn.getResponseCode()));
			}
        }
        catch(IOException ex){
        	ex.printStackTrace();
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        return stream;
    }
	
	private String parseXML(InputStream is)throws XmlPullParserException, IOException{
		//sv = (LinearLayout)findViewById(R.id.success);
		XmlPullParser parser = Xml.newPullParser();
    	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    	parser.setInput(is, null);
    	parser.nextTag();
    	parser.require(XmlPullParser.START_TAG, ns, "reply");
    	
    	String transNo = new String(),status = new String();
    	
    	while (parser.next() != XmlPullParser.END_TAG) {
    		if (parser.getEventType() != XmlPullParser.START_TAG) {
    			continue;
    		}
    		String name = parser.getName();
    		if(name.equals("transNo")){
    			parser.require(XmlPullParser.START_TAG, ns, "transNo");
    			if (parser.next() == XmlPullParser.TEXT) {
    				transNo = parser.getText();
    		        parser.nextTag();
    		    }
    			parser.require(XmlPullParser.END_TAG, ns, "transNo");
    			Log.d(DEBUG_TAG, "Date: "+transNo);
    		}
    		else if(name.equals("status")){
    			parser.require(XmlPullParser.START_TAG, ns, "status");
    			if (parser.next() == XmlPullParser.TEXT) {
    				status = parser.getText();
    		        parser.nextTag();
    		    }
    			parser.require(XmlPullParser.END_TAG, ns, "status");
    			Log.d(DEBUG_TAG, "Status: "+status);
    		}
    		else{
    			skip(parser);
    		}
    	}
    	
		return status;
	}
	
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
