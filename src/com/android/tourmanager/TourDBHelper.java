package com.android.tourmanager;

import com.android.tourmanager.TourContract.State;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TourDBHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "loanDatabase.db";
    public static SQLiteDatabase mDB;
    public static final String DEBUG_TAG = "TourDBHelper";
    private static final String TEXT_TYPE = " TEXT";
    private static ContentValues values = new ContentValues();
    
    private static final String SQL_CREATE_STATE =
    	    "CREATE TABLE " + State.TABLE_NAME + " (" +
    	    		State._ID + " INTEGER PRIMARY KEY," + 
    	    		State.SIGNATURE +TEXT_TYPE+" UNIQUE NOT NULL )";
    
    private static final String SQL_DELETE_STATE ="DROP TABLE IF EXISTS " + State.TABLE_NAME;

	public TourDBHelper(Context context) {
		super(context,DATABASE_NAME, null, DATABASE_VERSION);
		TourDBHelper.mDB = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_STATE);
		Log.d(DEBUG_TAG, "SQL for Employee:"+ SQL_CREATE_STATE);
		insertFirstSignature(db);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		db.execSQL(SQL_DELETE_STATE);

	}
	
	public Cursor getSignature(){
    	Cursor c = mDB.query(State.TABLE_NAME, new String[] {State._ID, State.SIGNATURE} ,
    			null, null, null, null,null);
    	return c;
    }
	
	private void insertFirstSignature(SQLiteDatabase db){
    	values.put(State.SIGNATURE, "0");
    	
    	// Insert the new row, returning the primary key value of the new row
    	long newRowId = db.insertWithOnConflict(
    			State.TABLE_NAME,
    			State.COLUMN_NAME_NULLABLE,
    			values,
    			SQLiteDatabase.CONFLICT_REPLACE);
    	Log.d(DEBUG_TAG, "New Signature Row Id:"+ newRowId+" "+"0");
    	Log.d(DEBUG_TAG, "New Signature: "+ "Signature"+"0");
    	values.clear();
    }

}
