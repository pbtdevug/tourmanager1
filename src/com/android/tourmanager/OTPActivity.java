package com.android.tourmanager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Vector;
import android.app.Dialog;
import android.app.ProgressDialog;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import com.android.tourmanager.TourContract.State;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OTPActivity extends Activity {
	private StringBuffer xmlStr = new StringBuffer();
	public static String pin,trxnID,checkPtID,trxnNum,otp;
	public static final String activityKey = "com.android.OTPActivity";
	private static final String DEBUG_TAG = "OTPActivity";
	private SharedPreferences sharedPrefs;
	private static String phoneID,simSerialNumber;
	public static TourDBHelper rDbHelper;
	private ManageServerConnections msc;
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	private ProgressDialog mProgressDialog;
	
	private static String transNo;
	private String status;
	private static final String ns = null;
	
	//private NetworkReceiver receiver;
	private Handler handler = new Handler(Looper.getMainLooper());
    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
 // Whether the display should be refreshed.
    public static boolean refreshDisplay = true;

    // The user's current network preference setting.
    public static String sPref = null;
    
    public static boolean wifiConnected = false;
    // Whether there is a mobile connection.
    public static boolean mobileConnected = false;
    
    public static String url = "http://www.askariplus.com/askari/backend/mobile/mobile.php";
    private Camera camera;
    private boolean isFlashOn;
    Parameters params;
    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_otp);
		
		Intent intent = getIntent();
		pin = intent.getStringExtra(PinActivity.activityKey);
		
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        phoneID = telephonyManager.getDeviceId();
        
        TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simSerialNumber = telemamanger.getSimSerialNumber();
        
        //setTitle(simSerialNumber);
        
        //get user set server preferences
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        //url = sharedPrefs.getString("server_url", "");
        
        msc = new ManageServerConnections();
	}
	
	@Override
	public void onStart(){
		super.onStart();

        updateConnectedFlags();
        
        //time out the
        handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );
	}
	
	@Override
    protected void onStop() {
        super.onStop();
         
        // on stop release the camera
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }
 
    @Override
    protected void onPause() {
        super.onPause();
         
        // on pause turn off the flash
        turnOffFlash();
    }
	
	@Override
    public void onDestroy() {
        super.onDestroy();
        /*if (receiver != null) {
            this.unregisterReceiver(receiver);
        }*/
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		rDbHelper = new TourDBHelper(getBaseContext());
		
		Cursor cursor = rDbHelper.getSignature();
		cursor.moveToFirst();
		String signature = cursor.getString(cursor.getColumnIndexOrThrow(State.SIGNATURE));
		if(signature.equals("0")){
			// Inflate the menu; this adds items to the action bar if it is present.
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.ot, menu);
			return super.onCreateOptionsMenu(menu);
	    }
		else{
			return false;
		}
	    
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.action_settings){
			Intent settingsActivity = new Intent(getBaseContext(), SettingsActivity.class);
            startActivity(settingsActivity);
            return true;
		}
		else{
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void updateConnectedFlags() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        } else {
            wifiConnected = false;
            mobileConnected = false;
        }
        Log.d(DEBUG_TAG, ""+wifiConnected);
        Log.d(DEBUG_TAG, ""+mobileConnected);
        Log.d(DEBUG_TAG, "DateTime"+(new Date()));
    }
	
	
	@SuppressLint("InflateParams")
	private void createDialog(){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(R.layout.prompts2, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final EditText otpInput = (EditText) promptsView.findViewById(R.id.otp);
		final EditText idInput = (EditText) promptsView.findViewById(R.id.checkPtId);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	otp = otpInput.getText().toString();
			    	checkPtID = idInput.getText().toString();
			    	trxnNum = getTrxnNumber();
			    	trxnID = trxnNum;
			    	if(checkPtID.length()==4){
			    		if(otp.length()==6){
			    			xmlStr = createXML(otp,checkPtID,pin,"now",trxnID);
			    			//startConnectionActivity(xmlStr.toString());
			    			msc.setUpdateVariable(xmlStr.toString());
			    			new DownloadJsonTask().execute(url);
					    	xmlStr.delete(0, xmlStr.length());
					    	turnOffFlash();
			    		}
			    		else{
			    			Toast.makeText(OTPActivity.this,"OTP must be 6 digits!", Toast.LENGTH_LONG).show();
			    		}
			    	}
			    	else{
			    		Toast.makeText(OTPActivity.this,"CheckPoint ID must be 4 digits!", Toast.LENGTH_LONG).show();
			    	}
			    }
			  }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog,int id) {
					  turnOffFlash();
					  dialog.cancel();
				  }
		});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
		//switch on flash
		checkTorchSupport();
    }
	
	/*private void startConnectionActivity(String xmlStr){
		Intent connIntent = new Intent(this, ConnectionActivity.class);
    	connIntent.putExtra(activityKey, xmlStr);
        startActivity(connIntent);
        Log.d(DEBUG_TAG, "started Connection Activity ");
        Log.d(DEBUG_TAG, "xmlStr = "+xmlStr);
	}*/
	
	public static String getTrxnNumber() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector<String> dateInfo = split((new Date()).toString()," ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(2)+mnt+(dateInfo.elementAt(5).toString().substring(2, 4))+(dateInfo.elementAt(3).toString().substring(0, 2))+(dateInfo.elementAt(3).toString().substring(3, 5))+(dateInfo.elementAt(3).toString().substring(6, 8));
        return txn;
    }
	
	public static Vector<String> split(String splitStr, String delimiter) {
		//String [] splitArray;
		StringBuffer token = new StringBuffer();
		Vector<String> tokens = new Vector<String>();

		// split
		char[] chars = splitStr.toCharArray();
		for (int i=0; i < chars.length; i++) {
			if (delimiter.indexOf(chars[i]) != -1) {
				// we bombed into a delimiter
				if (token.length() > 0) {
					tokens.addElement(token.toString());
					token.setLength(0);
				}
			}
			else {
				token.append(chars[i]);
			}
		}
		// don't forget the "tail"...
		if (token.length() > 0) {
			tokens.addElement(token.toString());
		}
        return tokens;
	}
	
	private void checkTorchSupport(){
		boolean hasFlash = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
		if (hasFlash==false) {
			Toast.makeText(OTPActivity.this,"Sorry, your device doesn't support flash light!", Toast.LENGTH_SHORT).show();
		}
		else{
			getCamera();
			turnOnFlash();
		}
		
	}
	
	private void getCamera() {
	    if (camera == null) {
	        try {
	            camera = Camera.open();
	            params = camera.getParameters();
	        } catch (RuntimeException e) {
	            Log.e("Camera Error. Failed to Open. Error: ","Error");
	        }
	    }
	}
	
	private void turnOnFlash() {
		int hour = Integer.parseInt(getHour());
		if(hour>18||hour<7){
			if (!isFlashOn) {
				if (camera == null || params == null) {
					return;
				}
	         
				params = camera.getParameters();
				params.setFlashMode(Parameters.FLASH_MODE_TORCH);
				camera.setParameters(params);
				camera.startPreview();
				isFlashOn = true;
			}
		}
	}
	
	private void turnOffFlash() {
		if (isFlashOn) {
			if (camera == null || params == null) {
				return;
			}
				
			params = camera.getParameters();
			params.setFlashMode(Parameters.FLASH_MODE_OFF);
			camera.setParameters(params);
			camera.stopPreview();
			isFlashOn = false;
		}
	}
	
	public void startDialog(View view){
		turnOnFlash();
		createDialog();
	}
	
	public static StringBuffer createXML(String opt,String chkptID,String pin,String status,String txNum){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<tourRecord>");
        xmlStr.append("<chkptID>").append(chkptID).append("</chkptID>");
        xmlStr.append("<otp>").append(opt).append("</otp>");
        xmlStr.append("<imei>").append(phoneID).append("</imei>");//
        xmlStr.append("<iccid>").append(simSerialNumber.substring(0, simSerialNumber.length()-1)).append("</iccid>");//
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("</tourRecord>");
        return xmlStr;
    }
	
	private static String getHour() {//Fri 29 Nov 2013
        Vector<String> dateInfo = split((new Date()).toString()," ");
        String time = dateInfo.elementAt(3).toString();
        String hour = split(time,":").elementAt(0).toString();
        Log.d(DEBUG_TAG, "Hour = "+hour);
        return hour;
    }
	
	private class DownloadJsonTask extends AsyncTask<String, Void, String> {

		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(DIALOG_DOWNLOAD_PROGRESS);
		}
		
        @Override
        protected String doInBackground(String... urls) {
                return loadJsonFromNetwork(urls[0]);
        }

        @SuppressWarnings("deprecation")
		@Override
        protected void onPostExecute(String result) {
        	dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        	showFeedBack(result);
        	//startMain();
        }
    }
	
	private String loadJsonFromNetwork(String urlString){
        InputStream stream = null;
        String status = "";
        //List<Entry> entries = null;

        // Checks whether the user set the preference to include summary text
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean pref = sharedPrefs.getBoolean("summaryPref", false);
        Log.d(DEBUG_TAG, String.valueOf(pref));
        
        try {
            stream = msc.downloadUrl(urlString);
            if(stream!=null){
            	try {
					status = parseXML(stream);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        }
        catch(IOException ex){
        	ex.printStackTrace();
        }
        finally {
            if (stream != null) {
            	try{
            		stream.close();
                }catch(IOException ex){
                	
                }
            }
        }
        return status;
    }
	
	private String parseXML(InputStream is)throws XmlPullParserException, IOException{
		//sv = (LinearLayout)findViewById(R.id.success);
		XmlPullParser parser = Xml.newPullParser();
    	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    	parser.setInput(is, null);
    	parser.nextTag();
    	parser.require(XmlPullParser.START_TAG, ns, "reply");
    	
    	transNo = new String();
    	status = new String();
    	
    	while (parser.next() != XmlPullParser.END_TAG) {
    		if (parser.getEventType() != XmlPullParser.START_TAG) {
    			continue;
    		}
    		String name = parser.getName();
    		if(name.equals("transNo")){
    			parser.require(XmlPullParser.START_TAG, ns, "transNo");
    			if (parser.next() == XmlPullParser.TEXT) {
    				transNo = parser.getText();
    		        parser.nextTag();
    		    }
    			parser.require(XmlPullParser.END_TAG, ns, "transNo");
    			Log.d(DEBUG_TAG, "Date: "+transNo);
    		}
    		else if(name.equals("status")){
    			parser.require(XmlPullParser.START_TAG, ns, "status");
    			if (parser.next() == XmlPullParser.TEXT) {
    				status = parser.getText();
    		        parser.nextTag();
    		    }
    			parser.require(XmlPullParser.END_TAG, ns, "status");
    			Log.d(DEBUG_TAG, "Status: "+status);
    		}
    		else{
    			skip(parser);
    		}
    	}
    	
    	if(status.equalsIgnoreCase("SUCCESSFUL")){
			//updateSignature();
		}
    	
		return status;
	}
	
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
	
	private void showFeedBack(String resp){
		if(resp.equalsIgnoreCase("SUCCESSFUL")){
			//Toast.makeText(ConnectionActivity.this,resp, Toast.LENGTH_LONG).show();
			createDialog(resp,R.layout.success_prompt,R.id.success);
		}
		else if(resp.equals("")){
			createDialog("Connection Timeout",R.layout.error_prompt,R.id.error);
		}
		else{
			//Toast.makeText(ConnectionActivity.this,resp, Toast.LENGTH_LONG).show();
			createDialog(resp,R.layout.error_prompt,R.id.error);
		}
		//ConfirmationActivity.activeTrxn.delete(0, ConfirmationActivity.activeTrxn.length());
    	Log.d(DEBUG_TAG, resp);
    }
	
	@SuppressLint("InflateParams")
	private void createDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView successTextView = (TextView) promptsView.findViewById(textView);
		successTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	
			    }
			  });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
	
	@Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
			case DIALOG_DOWNLOAD_PROGRESS:
				mProgressDialog = new ProgressDialog(this);
				mProgressDialog.setMessage("please wait");
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgressDialog.setCancelable(true);
				mProgressDialog.show();
				
				return mProgressDialog;default:
				return null;
        }
    }
}
