package com.android.tourmanager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.conn.ConnectTimeoutException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import com.android.tourmanager.TourContract.State;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ConnectionActivity extends Activity{
	public static final String DEBUG_TAG = "ConnectionActivity";
	private String xmlStr;
	private static final String ns = null;
	private FileOutputStream outputStream;
	private static ContentValues values = new ContentValues();
	int respCode = 0;
	private static String transNo;
	private String status;
	private static TourDBHelper rDbHelper;
	private static SQLiteDatabase db;
	//private Handler handler = new Handler(Looper.getMainLooper());
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connection);
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	// Refreshes the display if the network connection and the
    // pref settings allow it.
    @Override
    public void onStart(){
        super.onStart();
        Log.d(DEBUG_TAG, "started Connection.....: ");
        Intent intent = getIntent();
        xmlStr = intent.getStringExtra(OTPActivity.activityKey);
        Log.d(DEBUG_TAG, "Request:"+xmlStr);
        
        // Only loads the page if refreshDisplay is true. Otherwise, keeps previous
        // display. For example, if the user has set "Wi-Fi only" in prefs and the
        // device loses its Wi-Fi connection midway through the user using the app,
        // you don't want to refresh the display--this would force the display of
        // an error page instead of stackoverflow.com content.
        if (OTPActivity.refreshDisplay) {
        	Log.d(DEBUG_TAG, "loading page: ");
        	String url = /*OTPActivity.url*/"http://www.askariplus.com/askari/backend/mobile/mobile.php";
        	Log.d(DEBUG_TAG, "URL: "+url);
            loadPage(url);
            setContentView(R.layout.activity_connection);
        }
        
        /*handler.postDelayed(new Runnable() {
			@Override
			public void run() {
        		if(respCode==0){
        			showFeedBack("");
        		}
			  }
			}, 20000 );*/
    }

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.connection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void loadPage(String connURL) {
		
		//if (((OTPActivity.sPref.equals(OTPActivity.ANY)) && (OTPActivity.wifiConnected || OTPActivity.mobileConnected))|| ((OTPActivity.sPref.equals(OTPActivity.WIFI)) && (OTPActivity.wifiConnected))) {
            // AsyncTask subclass
            new DownloadJsonTask().execute(connURL);
            Log.d(DEBUG_TAG, connURL);
        /*} else {
            showErrorPage();
        }*/
        Log.d(DEBUG_TAG, "wifiConnected: "+OTPActivity.wifiConnected);
        Log.d(DEBUG_TAG, "mobileConnected: "+OTPActivity.mobileConnected);
        Log.d(DEBUG_TAG, "pref: "+OTPActivity.sPref);
    }
	
	/*private void showErrorPage() {
		String trxnID = OTPActivity.trxnID;
		xmlStr = OTPActivity.createXML(OTPActivity.otp,OTPActivity.checkPtID,OTPActivity.pin,"past",trxnID).toString();
		writeToFile(trxnID.trim()+".txt",Context.MODE_PRIVATE,xmlStr);
		
		/*ArrayList<String> pending = getPending();
		pending.add(trxnID.trim());
		Log.d("ConnectionActivity", "pendingList = " + pending);
		writeToFile("pending.txt",Context.MODE_PRIVATE,pending.toString());*/
		
		//ConfirmationActivity.activeTrxn.delete(0, ConfirmationActivity.activeTrxn.length());
		//Toast.makeText(ConnectionActivity.this,"Transaction saved for later submission", Toast.LENGTH_SHORT).show();
		
		/*WebView myWebView = new WebView(this);
        myWebView.loadData(getResources().getString(R.string.connection_error),"text/html", null);
        setContentView(myWebView);
    }*/
	
	private class DownloadJsonTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
                return loadJsonFromNetwork(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
        	showFeedBack(result);
        	//startMain();
        }
    }
	
	private String loadJsonFromNetwork(String urlString){
        InputStream stream = null;
        String status = "";
        //List<Entry> entries = null;

        // Checks whether the user set the preference to include summary text
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean pref = sharedPrefs.getBoolean("summaryPref", false);
        Log.d(DEBUG_TAG, String.valueOf(pref));
        
        try {
            stream = downloadUrl(urlString);
            if(stream!=null){
            	try {
					status = parseXML(stream);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        }
        catch(IOException ex){
        	ex.printStackTrace();
        }
        finally {
            if (stream != null) {
            	try{
            		stream.close();
                }catch(IOException ex){
                	
                }
            }
        }
        return status;
    }
	
	private InputStream downloadUrl(String urlString) throws IOException {
		InputStream stream = null;
		try{
			Log.d(DEBUG_TAG, "URL = "+urlString);
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			OutputStream os = new BufferedOutputStream(conn.getOutputStream());
			os.write(xmlStr.getBytes());
			os.flush();
			os.close();
			conn.connect();
			respCode = conn.getResponseCode();
			Log.d(DEBUG_TAG, "Connection open");
			Log.d(DEBUG_TAG, String.valueOf(conn.getResponseCode()));
			if(respCode==200){
				stream = new BufferedInputStream(conn.getInputStream());
				//Log.d(DEBUG_TAG,"Resp"+convertToStr(stream));
				Log.d(DEBUG_TAG, "got inputstream");
				Log.d(DEBUG_TAG, String.valueOf(conn.getResponseCode()));
			}
        }
		catch(ConnectTimeoutException ex){
			showFeedBack("");
			ex.printStackTrace();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
        return stream;
    }
	
	private void showFeedBack(String resp){
		if(resp.equalsIgnoreCase("SUCCESSFUL")){
			//Toast.makeText(ConnectionActivity.this,resp, Toast.LENGTH_LONG).show();
			createDialog(resp,R.layout.success_prompt,R.id.success);
		}
		else if(resp.equals("")){
			String trxnID = OTPActivity.trxnID;
			xmlStr = OTPActivity.createXML(OTPActivity.otp,OTPActivity.checkPtID,OTPActivity.pin,"past",trxnID).toString();
			writeToFile(trxnID.trim()+".txt",Context.MODE_PRIVATE,xmlStr);
			createDialog("Connection Timeout",R.layout.error_prompt,R.id.error);
		}
		else{
			//Toast.makeText(ConnectionActivity.this,resp, Toast.LENGTH_LONG).show();
			createDialog(resp,R.layout.error_prompt,R.id.error);
		}
		//ConfirmationActivity.activeTrxn.delete(0, ConfirmationActivity.activeTrxn.length());
    	Log.d(DEBUG_TAG, resp);
    }
	
	/*private String convertToStr(InputStream is) throws IOException{
    	int ch ;
        StringBuffer sb = new StringBuffer();
        while((ch = is.read())!=-1){
            sb.append((char)ch);
        }
        return sb.toString();
    }*/
	
	private void startMain(){
		finish();
	}
	
	private String parseXML(InputStream is)throws XmlPullParserException, IOException{
		//sv = (LinearLayout)findViewById(R.id.success);
		XmlPullParser parser = Xml.newPullParser();
    	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    	parser.setInput(is, null);
    	parser.nextTag();
    	parser.require(XmlPullParser.START_TAG, ns, "reply");
    	
    	transNo = new String();
    	status = new String();
    	
    	while (parser.next() != XmlPullParser.END_TAG) {
    		if (parser.getEventType() != XmlPullParser.START_TAG) {
    			continue;
    		}
    		String name = parser.getName();
    		if(name.equals("transNo")){
    			parser.require(XmlPullParser.START_TAG, ns, "transNo");
    			if (parser.next() == XmlPullParser.TEXT) {
    				transNo = parser.getText();
    		        parser.nextTag();
    		    }
    			parser.require(XmlPullParser.END_TAG, ns, "transNo");
    			Log.d(DEBUG_TAG, "Date: "+transNo);
    		}
    		else if(name.equals("status")){
    			parser.require(XmlPullParser.START_TAG, ns, "status");
    			if (parser.next() == XmlPullParser.TEXT) {
    				status = parser.getText();
    		        parser.nextTag();
    		    }
    			parser.require(XmlPullParser.END_TAG, ns, "status");
    			Log.d(DEBUG_TAG, "Status: "+status);
    		}
    		else{
    			skip(parser);
    		}
    	}
    	
    	if(status.equalsIgnoreCase("SUCCESSFUL")){
			updateSignature();
		}
    	
		return status;
	}
	
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
	
	public void saveTrxn(String content,String fileName){
		try {
    		outputStream = openFileOutput(fileName, Context.MODE_APPEND);
    		outputStream.write(content.getBytes());
    		outputStream.close();
    	} 
    	catch (Exception e) {
    		Log.d("MainActivity", "Error " + e.toString());
    		e.printStackTrace();
    	}
	}
	
	private void writeToFile(String filename,int mode,String content){
		try {
			//Log.d("Conn-WriteToFile", "Conents = " + content);
			Log.d("Conn-WriteToFile", "FileName = " + filename);
			FileOutputStream outputStream = openFileOutput(filename, mode);
			outputStream.write(content.getBytes());
    		outputStream.close();
    	} 
    	catch (Exception e) {
    		Log.d("conn-writeToFile", "Error " + e.toString());
    		e.printStackTrace();
    	}
    }
	
	@SuppressLint("InflateParams")
	private void createDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView successTextView = (TextView) promptsView.findViewById(textView);
		successTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	startMain();
			    }
			  });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
	
	private static void updateSignature(){
		rDbHelper = OTPActivity.rDbHelper;
        
        db = rDbHelper.getWritableDatabase();
		values.put(State.SIGNATURE, transNo);
		int count = db.update(
		    State.TABLE_NAME,
		    values,
		    null,
		    null);
		
		Log.d(DEBUG_TAG, "Update= : "+count);
		values.clear();
	}
}