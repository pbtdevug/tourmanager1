package com.android.tourmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootUpReceiver extends BroadcastReceiver {
	public static final String DEBUG_TAG = "BootUpReceiver";

	@Override
	public void onReceive(Context context, Intent intent) { 
		Intent serviceIntent = new Intent(context, LogerService.class);
		serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startService(serviceIntent);
        
        Log.d(DEBUG_TAG, "reciever invoked");
        
        
	}

}
